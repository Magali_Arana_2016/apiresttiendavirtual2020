require('events').EventEmitter.defaultMaxListeners = Infinity;
var pgRc = require('pg');
var sqlRc = require('mssql');
var conexionRc = require('./connection/connectionRcPg.js');
var poolRc = new pgRc.Pool(conexionRc);
var service = require('./services');
//require('events').EventEmitter.prototype._maxListeners = 100;
module.exports = function(app) {
    app.post('/wsRCPGE/listarCategoriasProductosEncripta', listarCategoriasProductosEncripta);
    app.post('/wsRCPGE/fechaHora', fechaHora);
    app.post('/wsRCPGE/listarProductos', listarProductos);
    app.post('/wsRCPGE/eliminaProducto', eliminaProducto);
    app.post('/wsRCPGE/modificarProducto', modificarProducto);
    app.post('/wsRCPGE/listarProductosPorOid', listarProductosPorOid);
    app.post('/wsRCPGE/listarProductosPorIdAe', listarProductosPorIdAe);
    app.post('/wsRCPGE/activarProducto', activarProducto);
    app.post('/wsRCPGE/desactivarProducto', desactivarProducto);
    app.post('/wsRCPGE/listarProductosPorIdAe_Oid', listarProductosPorIdAe_Oid);
    app.post('/wsRCPGE/activar_tramitePe', activar_tramitePe);
    app.post('/wsRCPGE/adicionarProducto_ae', adicionarProducto_ae);
    app.post('/wsRCPGE/listarProductos_ae', listarProductos_ae);
    app.post('/wsRCPGE/listarProductosPorIdAe_Oid_Ae', listarProductosPorIdAe_Oid_Ae);
    app.post('/wsRCPGE/eliminaProducto_ae', eliminaProducto_ae);
    app.post('/wsRCPGE/modificarProducto_ae', modificarProducto_ae);
    app.post('/wsRCPGE/listarProductosPorIdAe_ae', listarProductosPorIdAe_ae);
    app.post('/wsRCPGE/listarProductosPorOid_ae', listarProductosPorOid_ae);
    app.post('/wsRCPGE/listarProductosPorIdTv_ae', listarProductosPorIdTv_ae);
    app.post('/wsRCPGE/listarTiendaVirtualPorIdAe', listarTiendaVirtualPorIdAe);
    app.post('/wsRCPGE/listarPaginaWebPorIdAe', listarPaginaWebPorIdAe);
    app.post('/wsRCPGE/activarPublicacionPaginaWeb', activarPublicacionPaginaWeb);
    app.post('/wsRCPGE/desactivarPublicacionPaginaWeb', desactivarPublicacionPaginaWeb);
    app.post('/wsRCPGE/adicionarPaginaWeb', adicionarPaginaWeb);
    app.post('/wsRCPGE/modificarPaginaWeb', modificarPaginaWeb);
    app.post('/wsRCPGE/darBajaVenta', darBajaVenta);
    app.post('/wsRCPGE/validarVenta', validarVenta);
    app.post('/wsRCPGE/valorarAe', valorarAe);
    app.post('/wsRCPGE/listarVentasValoracion', listarVentasValoracion);
    app.post('/wsRCPGE/listarCategoriasProductos', listarCategoriasProductos);
    app.post('/wsRCPGE/listarCategoriasProductos2', listarCategoriasProductos2);
    app.post('/wsRCPGE/listadoCategoriaP', listadoCategoriaP);
    app.post('/wsRCPGE/addtiendavirtual', addtiendavirtual);
    app.post('/wsRCPGE/insertarProductoVentas', insertarProductoVentas);
    app.post('/wsRCPGE/updatetiendavirtual', updatetiendavirtual);
    app.post('/wsRCPGE/listadoVirtualae', listadoVirtualae);
    app.post('/wsRCPGE/listarProductos20', listarProductos20);
    app.post('/wsRCPGE/actEstadoVenta', actualizaEstadoVenta);
    app.post('/wsRCPGE/getEstadoProducto', getEstadoProducto);
    app.post('/wsRCPGE/getVersionApp', getVersionAppAndroid); 
    app.post('/wsRCPGE/verificaDominio', verificaDominio);
    app.post('/wsRCPGE/insertarDenunciaAE', insertarDenunciaAE);
    app.post('/wsRCPGE/listarAE', listarTiendasVirtuales);

    //Tienda Virtual   v001
    app.get('*', function(req, res) {
        res.sendfile('./index.html'); // Carga única de la vista
    });
};

//autenticacion
function ejecutarProductosQuery(dataQuery, res) {
    poolRc.connect(function(err, client, done) {
        if (err) {
            res.json({ "error": { "message": "error de conexion: " + err, "code": 603 } });
        } else {
            client.query(dataQuery, function(err, result) {
                done();
                if (err) {
                    var datos = { "error": { "message": "error de consulta: " + err, "code": 601 } };
                    datos = service.encripta(JSON.stringify(datos));
					res.json(datos);
                } else {
                    //res.json(result.rows);
                    var stam = result.rows.length;
                    var datos = { "success": result.rows };
                    datos = service.encripta(JSON.stringify(datos));
                    res.json(datos);
                    //res.json(result.rows);
                    /*}else{
                      res.json({"error": { "message": "No se encontraron registros", "code": 602 }});
                    }*/
                }
            });
        }
    });
    poolRc.on('error', function(err, client) {
        res.json({ "error": { "message": "error de instancia: " + err, "code": 603 } });
    });
};

function listarCategoriasProductosEncripta(req, res) {
    try {
        var dataQuery = "SELECT * FROM cat_listar_ae();";
        ejecutarProductosQuery(dataQuery, res);
    } catch (e) {
        res.json({ "Error": { "code": 500 } });
    }
};


function fechaHora(req, res) {
    try {
        var fecha = new Date();
        var fechaactul = "";
        var smes = fecha.getMonth() + 1;

        smes = (smes < 10) ? '0' + smes : smes;
        fechactual = fecha.getFullYear() + "-" + smes + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();

        res.json({ "success": { "fecha": fechactual, "code": 200 } });
    } catch (e) {
        console.log("Error al generar Fecha.");
        res.json({ "Error": { "code": 500 } });
    }
};
function listarProductos(req, res) {
    var dataQuery = "SELECT * FROM prd_listar();";
    ejecutarProductosQuery(dataQuery, res);
};
function eliminaProducto(req, res) {
    var prd_idc = req.body.prd_idc;
    var dataQuery = "SELECT * FROM prd_elimina(" + prd_idc + ")";
    console.log('dataQuery', dataQuery);

    ejecutarProductosQuery(dataQuery, res);
};
function modificarProducto(req, res) {
    var prd_idc = req.body.prd_idc;
    var prd_nombrec = req.body.prd_nombrec;
    var prd_descripcionc = req.body.prd_descripcionc;
    var prd_precioc = req.body.prd_precioc;
    var prod_aec = req.body.prod_aec;
    var prod_sucursalc = req.body.prod_sucursalc;
    var prd_marcac = req.body.prd_marcac;
    var prd_categoriac = req.body.prd_categoriac;
    var prd_imagen_pc = req.body.prd_imagen_pc;
    var prd_imagen_a1c = req.body.prd_imagen_a1c;
    var prd_imagen_a2c = req.body.prd_imagen_a2c;
    var prd_oidc = req.body.prd_oidc;
    var prd_telefono_referenciac = req.body.prd_telefono_referenciac;
    var prd_usrc = req.body.prd_usrc;
    var dataQuery = "SELECT * FROM prd_modifica(" + prd_idc + ",'" + prd_nombrec + "', '" + prd_descripcionc + "', " + prd_precioc + ", " + prod_aec + "," + prod_sucursalc + ", '" + prd_marcac + "','" + prd_categoriac + "','" + prd_imagen_pc + "','" + prd_imagen_a1c + "','" + prd_imagen_a2c + "','" + prd_oidc + "','" + prd_telefono_referenciac + "','" + prd_usrc + "')";
    console.log('dataQuery', dataQuery);
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorOid(req, res) {
    var oid_ciu = req.body.oid_ciu;

    var dataQuery = "SELECT * FROM prd_listarPorOid('" + oid_ciu + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorIdAe(req, res) {
    var id_ae = req.body.id_ae;

    var dataQuery = "SELECT * FROM prd_listarporidae(" + id_ae + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function activarProducto(req, res) {
    var prd_idc = req.body.prd_idc;
    var dataQuery = "SELECT * FROM prd_cambia_estado_a_activo(" + prd_idc + ")";
    console.log('dataQuery', dataQuery);

    ejecutarProductosQuery(dataQuery, res);
};
function desactivarProducto(req, res) {
    var prd_idc = req.body.prd_idc;
    var dataQuery = "SELECT * FROM prd_cambia_estado_a_inactivo(" + prd_idc + ")";
    console.log('dataQuery', dataQuery);

    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorIdAe_Oid(req, res) {
    var id_ae = req.body.id_ae;
    var oidc = req.body.oidc;
    var dataQuery = "SELECT * FROM prd_listarporidae_oid(" + id_ae + ",'" + oidc + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function activar_tramitePe(req, res) {
    var tramite = req.body.tramite;
    var idae = req.body.idae;
    var oid = req.body.oid;
    var if_codigo = req.body.if_codigo;
    var dataQuery = "SELECT * FROM prd_activar_tramite_pe(" + tramite + "," + idae + ",'" + oid + "','" + if_codigo + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function adicionarProducto_ae(req, res) {
    var prd_tv_idc = req.body.prd_tv_idc;
    var prd_nombrec = req.body.prd_nombrec;
    var prd_descripcionc = req.body.prd_descripcionc;
    var prd_precioc = req.body.prd_precioc;
    var prd_imagen_pc = req.body.prd_imagen_pc;
    var prd_imagen_a1c = req.body.prd_imagen_a1c;
    var prd_imagen_a2c = req.body.prd_imagen_a2c;
    var prd_usrc = req.body.prd_usrc;
    var prd_oidc = req.body.prd_oidc;
    var prd_ofertac = req.body.prd_ofertac;
    var prd_descripcion_ofertac = req.body.prd_descripcion_ofertac;
    var prd_oferta_defechac = req.body.prd_oferta_defechac;
    var prd_oferta_hastafechac = req.body.prd_oferta_hastafechac;

    var dataQuery = "SELECT * FROM prod_adicionar_ae(" + prd_tv_idc + ",'" + prd_nombrec + "', '" + prd_descripcionc + "', " + prd_precioc + ", '" + prd_imagen_pc + "','" + prd_imagen_a1c + "','" + prd_imagen_a2c + "','" + prd_usrc + "','" + prd_oidc + "','" + prd_ofertac + "','" + prd_descripcion_ofertac + "','" + prd_oferta_defechac + "','" + prd_oferta_hastafechac + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductos_ae(req, res) {
    var dataQuery = "SELECT * FROM prd_listar_ae();";
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorIdAe_Oid_Ae(req, res) {
    var idAe = req.body.idAe;
    var oidc = req.body.oidc;
    var dataQuery = "SELECT * FROM prd_listar_por_oid_idae(" + idAe + ",'" + oidc + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorIdAe_ae(req, res) {
    var idAe = req.body.idAe;

    var dataQuery = "SELECT * FROM prd_listar_por_idae(" + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorOid_ae(req, res) {
    var oidc = req.body.oidc;

    var dataQuery = "SELECT * FROM prd_listar_por_oid('" + oidc + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function eliminaProducto_ae(req, res) {
    var prd_idc = req.body.prd_idc;
    var dataQuery = "SELECT * FROM prd_elimina_ae(" + prd_idc + ")";
    console.log('dataQuery', dataQuery);

    ejecutarProductosQuery(dataQuery, res);
};
function listarProductosPorIdTv_ae(req, res) {
    var idTv = req.body.idTv;
    var dataQuery = "SELECT * FROM prd_listar_por_idTv(" + idTv + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function modificarProducto_ae(req, res) {
    var prd_idc = req.body.prd_idc;
    var prd_tv_idc = req.body.prd_tv_idc;
    var prd_nombrec = req.body.prd_nombrec;
    var prd_descripcionc = req.body.prd_descripcionc;
    var prd_precioc = req.body.prd_precioc;
    var prd_imagen_pc = req.body.prd_imagen_pc;
    var prd_imagen_a1c = req.body.prd_imagen_a1c;
    var prd_imagen_a2c = req.body.prd_imagen_a2c;
    var prd_usrc = req.body.prd_usrc;
    var prd_ofertac = req.body.prd_ofertac;
    var prd_descripcion_ofertac = req.body.prd_descripcion_ofertac;
    var prd_oferta_defechac = req.body.prd_oferta_defechac;
    var prd_oferta_hastafechac = req.body.prd_oferta_hastafechac;
    var dataQuery = "SELECT * FROM prd_modifica_ae(" + prd_idc + "," + prd_tv_idc + ",'" + prd_nombrec + "','" + prd_descripcionc + "', " + prd_precioc + ", '" + prd_imagen_pc + "','" + prd_imagen_a1c + "','" + prd_imagen_a2c + "','" + prd_usrc + "','" + prd_ofertac + "','" + prd_descripcion_ofertac + "','" + prd_oferta_defechac + "','" + prd_oferta_hastafechac + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function listarTiendaVirtualPorIdAe(req, res) {
    var idAe = req.body.idAe;
    var dataQuery = "SELECT * FROM tv_listado_por_idae(" + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function listarPaginaWebPorIdAe(req, res) {
    var idAe = req.body.idAe;
    var dataQuery = "SELECT * FROM web_istado_por_idae(" + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function activarPublicacionPaginaWeb(req, res) {
    var idWeb = req.body.idWeb; 
    var idAe = req.body.idAe;
    var dataQuery = "SELECT * FROM web_activarPublicacion(" + idWeb + "," + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function desactivarPublicacionPaginaWeb(req, res) {
    var idWeb = req.body.idWeb;
    var idAe = req.body.idAe;
    var dataQuery = "SELECT * FROM web_desactivarPublicacion(" + idWeb + "," + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function listarProductos20(req, res) {
    var dataQuery = "SELECT * FROM cat_listar_20();";
    ejecutarProductosQuery(dataQuery, res);
};
function adicionarPaginaWeb(req, res) {
    var contenido = req.body.web_contenido;
    var url = req.body.web_url;
    var usuario = req.body.web_usuario;
    var idAe = req.body.web_idae;
    var dataQuery = "SELECT * FROM sp_adicionar_pagina_web('" + contenido + "' ,'" + url + "' ,'" + usuario + "' ," + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function modificarPaginaWeb(req, res) {
    var id = req.body.web_id;
    var contenido = req.body.web_contenido;
    var url = req.body.web_url;
    var estado = req.body.web_estado;
    var usuario = req.body.web_usuario;
    var idAe = req.body.web_idae;
    var dataQuery = "SELECT * FROM sp_modificar_pagina_web(" + id + " ,'" + contenido + "' ,'" + url + "','" + estado + "' ,'" + usuario + "' ," + idAe + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function validarVenta(req, res) {
    var val_idc = req.body.val_idc;
    var venta_idc = req.body.venta_idc;
    var dataQuery = "SELECT * FROM ventas_valida_ventas(" + val_idc + " ," + venta_idc + ");";
    console.log(dataQuery);
    ejecutarProductosQuery(dataQuery, res);
};
function valorarAe(req, res) {
    var val_idc = req.body.val_idc;
    var venta_idc = req.body.venta_idc;
    var puntaje = req.body.puntaje;
    var descripcion = req.body.descripcion;
    var dataQuery = "SELECT * FROM ventas_valora_ae(" + val_idc + " ," + venta_idc + "," + puntaje + ",'" + descripcion + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function darBajaVenta(req, res) {
    var vv_idc = req.body.vv_idc;
    var vnt_idc = req.body.vnt_idc;
    var dataQuery = "SELECT * FROM dar_baja_venta(" + vv_idc + " ," + vnt_idc + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function listarVentasValoracion(req, res) {
    var id_venta = req.body.id_venta;
    var telefono = req.body.telefono;
    var dataQuery = "SELECT * FROM public.sp_lst_valoracion(" + id_venta + " ," + telefono + ");";
    ejecutarProductosQuery(dataQuery, res);
};
function listarCategoriasProductos(req, res) {
    var dataQuery = "SELECT * FROM cat_listar_ae();";
    console.log('dataQuery', dataQuery);
    ejecutarProductosQuery(dataQuery, res);
};
function listarCategoriasProductos2(req, res) {
    var dataQuery = "SELECT * FROM cat_listar_ae2();";
    ejecutarProductosQuery(dataQuery, res);
};
function listadoCategoriaP(req, res) {
    try {
        var dataQuery = "SELECT * FROM splistadocategoria();";
        ejecutarProductosQuery(dataQuery, res);
    } catch (e) {
        res.json({ "Error": { "code": 500 } });
    }
};
function insertarProductoVentas(req, res) {
    var id_actividadeconomica = req.body.id_actividadeconomica;
    var total = req.body.total;
    var detalle = JSON.stringify(req.body.detalle);
    var nom = req.body.nom;
    var ap = req.body.ap;
    var tel = req.body.tel;
    var mail = req.body.mail;
    var dir = req.body.dir;
    var lat = req.body.lat;
    var long = req.body.long;
    var dataQuery = "SELECT * FROM sp_insertar_productos_ventas(" + id_actividadeconomica + ", " + total + ", '" + detalle + "', '" + nom + "', '" + ap + "', '" + tel + "', '" + mail + "', '" + dir + "', '" + lat + "', '" + long + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function addtiendavirtual(req, res) {
    try {
        var stv_ae_id = req.body.stv_ae_id;
        var stv_categoria_id = req.body.stv_categoria_id;
        var stv_nombre = req.body.stv_nombre;
        var stv_correo = req.body.stv_correo;
        var stv_pagina_web = req.body.stv_pagina_web;
        var stv_descripcion = req.body.stv_descripcion;
        var stv_contactos = req.body.stv_contactos;
        var stv_redes_sociales = req.body.stv_redes_sociales;
        var stv_oid = req.body.stv_oid;
        var stv_usr = req.body.stv_usr;
        var stv_catalogo = req.body.stv_catalogo;
        var stv_logotipo = req.body.stv_logotipo;
        var stv_encabezado = req.body.stv_encabezado;
        var stv_forma_entrega = req.body.stv_forma_entrega;
        var stv_horarios = req.body.stv_horarios;
        var stv_dominio = req.body.stv_dominio;
        var dataQuery = "SELECT * FROM sp_adicionar_tienda_virtual(" + stv_ae_id + "," + stv_categoria_id + " ,'" + stv_nombre + "','" + stv_correo + "','" + stv_pagina_web + "','" + stv_descripcion + "','" + stv_contactos + "','" + stv_redes_sociales + "','" + stv_oid + "','" + stv_usr + "', '" + stv_catalogo + "','" + stv_logotipo + "','" + stv_encabezado + "','" + stv_forma_entrega + "','" + stv_horarios + "','" + stv_dominio + "');";
        ejecutarProductosQuery(dataQuery, res);
    } catch (e) {
        res.json({ "Error": { "code": 500 } });
    }
};
function updatetiendavirtual(req, res) {
    try {
        var idtv = req.body.idtv;
        var stv_ae_id = req.body.stv_ae_id;
        var stv_categoria_id = req.body.stv_categoria_id;
        var stv_nombre = req.body.stv_nombre;
        var stv_correo = req.body.stv_correo;
        var stv_pagina_web = req.body.stv_pagina_web;
        var stv_descripcion = req.body.stv_descripcion;
        var stv_contactos = req.body.stv_contactos;
        var stv_redes_sociales = req.body.stv_redes_sociales;
        var stv_oid = req.body.stv_oid;
        var stv_usr = req.body.stv_usr;
        var stv_catalogo = req.body.stv_catalogo;
        var stv_logotipo = req.body.stv_logotipo;
        var stv_encabezado = req.body.stv_encabezado;
        var stv_forma_entrega = req.body.stv_forma_entrega;
        var stv_horarios = req.body.stv_horarios;
        var stv_dominio = req.body.stv_dominio;
        var dataQuery = "SELECT * FROM sp_update_tienda_virtual(" + idtv + " ," + stv_ae_id + "," + stv_categoria_id + " ,'" + stv_nombre + "','" + stv_correo + "','" + stv_pagina_web + "','" + stv_descripcion + "','" + stv_contactos + "','" + stv_redes_sociales + "','" + stv_oid + "','" + stv_usr + "','" + stv_catalogo + "', '" + stv_logotipo + "', '" + stv_encabezado + "','" + stv_forma_entrega + "','" + stv_horarios + "','" + stv_dominio +"');";
        ejecutarProductosQuery(dataQuery, res);
    } catch (e) {
        res.json({ "Error": { "code": 500 } });
    }
};
function listadoVirtualae(req, res) {
    var idCiudadano = req.body.idCiudadano;
    var dataQuery = "SELECT * FROM splistadovirtualae('" + idCiudadano + "');";
    ejecutarProductosQuery(dataQuery, res);
};
function actualizaEstadoVenta(req, res) {
    var ven_idc = req.body.ven_idc;
    var ven_estado = req.body.ven_estado;
    var ven_obs = req.body.ven_obs;
    var dataQuery = "SELECT * FROM sp_update_estado_venta(" + ven_idc + ",'" + ven_estado + "','" + ven_obs + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function getEstadoProducto(req, res) {
    var ven_idc = req.body.idVenta;
    var dataQuery = "SELECT vnt_id, vnt_estado_pago FROM _ventas WHERE vnt_id = " + ven_idc + ";";
    ejecutarProductosQuery(dataQuery, res);
};
function getVersionAppAndroid(req, res) {
    var version = req.body.version;
    var dataQuery = "SELECT * FROM sp_verificar_vers_android('" + version + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function verificaDominio(req, res) {
    var dominio = req.body.dominio;
    var dataQuery = "SELECT * FROM tv_verificar_dominio('" + dominio + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function insertarDenunciaAE(req, res) {
    var d_ae_nombre = req.body.d_ae_nombre;
    var d_ae_realiza = req.body.d_ae_realiza;
    var d_ae_direccion = req.body.d_ae_direccion;
    var d_ae_latitud = req.body.d_ae_latitud;
    var d_ae_longitud = req.body.d_ae_longitud;
    var d_ae_descripcion = req.body.d_ae_descripcion;
    var d_ae_foto = req.body.d_ae_foto;
    //var detalle = JSON.stringify(req.body.detalle);
    var dataQuery = "SELECT * FROM sp_insertar_denuncias_ae('" + d_ae_nombre + "', '" + d_ae_realiza + "', '" + d_ae_direccion + "', '" + d_ae_latitud + "', '" + d_ae_longitud + "', '" + d_ae_descripcion + "', '" + d_ae_foto + "')";
    ejecutarProductosQuery(dataQuery, res);
};
function listarTiendasVirtuales(req, res) {
    var dominio = req.body.dominio;
    var dataQuery = "SELECT * FROM prd_listar_por_idtvjson1()";
    ejecutarProductosQuery(dataQuery, res);
};
