// services.js
var jwt = require('jwt-simple');  
var moment = require('moment');  
var config = require('./config');
var CryptoJS = require('crypto-js');
var fecha = new Date();
var key = 'comercio_electronico'; 


exports.encripta = function(obj)
{
	var encrypted = CryptoJS.AES.encrypt(obj, key);
	encrypted = encrypted.toString(); 
	return encrypted;
};

exports.desencripta = function(obj)
{
    var decrypted = CryptoJS.AES.decrypt(obj, key);
    decrypted = decrypted.toString(CryptoJS.enc.Utf8);
    return decrypted;
};
