var mongoose = require('mongoose');
module.exports = mongoose.model('htc_ciudadanos', {
	dtspslh_id: Object,	
    dtspslh_fecha_modificado : String,
    dtspslh_usuario : String,
	dtspslh_equipo : String,
	dtspslh_usuario_ip : String,
	dtspslh_servidor_ip : String,
	dtspslh_sistema_modificador : String,
	dtspslh_data: Object
});
