﻿var compression = require('compression');
var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var url = require('url');  
var fs = require('fs'); 

var port  	 = process.env.PORT || 8010;
app.use(compression());
app.configure(function() {
	app.use(express.static(__dirname )); 		
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
 	app.use(function(req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
		res.setHeader('Access-Control-Allow-Credentials', true);
		next();
	});
    
	app.get('/', function(req, res, next) {
	  // Handle the get for this route
	});

	app.post('/', function(req, res, next) {
	 // Handle the post for this route
	});

	app.put('/', function(req, res, next) {
	 // Handle the post for this route
	});
});

var sUrl  = '/opt/dreamfactory-1.9.4-2/apps/dreamfactory/htdocs/web/dreamfactory/dist/generadorPaginas';
app.use(express.static(__dirname + sUrl));

//SERVICIOS PROXY
app.get('/paginas/*', function (req, res,next) {
    try {
		var queryString = url.parse(req.url, true); 	
		if (req.path.indexOf('.') != -1) {
			var file = sUrl + req.path; 
			fs.exists(file, function (exists){ 
				console.log("exists :", exists);
				if (exists) {
					res.sendfile(file);
				}else{
					res.sendfile('index.html');
				}
			}); 
		}
    } catch (e) {
        res.json({ "Error": { "code": 500 } });
    }
});

require('./app/routesProductos.js')(app);
require('./app/routesEComerce.js')(app);
app.listen(port);
console.log("APP por el puerto " + port);
